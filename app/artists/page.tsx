'use client'

import { createClient } from '@/utils/supabase/client'
import { useEffect, useState } from 'react'

export default function Page() {
  const [artists, setArtists] = useState<any[] | null>(null)
  const supabase = createClient()

  useEffect(() => {
    const getData = async () => {
      const { data } = await supabase.from('artists')
        .select('artist')
        .order('artist', { ascending: true})
      setArtists(data)
    }
    getData()
  }, [supabase])

  return <pre>{JSON.stringify(artists, null, 2)}</pre>
}