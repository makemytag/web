'use client'

import { createClient } from '@/utils/supabase/client'
import { useEffect, useState } from 'react'

export default function Page() {
  const [albums, setAlbums] = useState<any[] | null>(null)
  const supabase = createClient()

  useEffect(() => {
    const getData = async () => {
      const { data } = await supabase.from('albums')
        .select('album, artist, year')
        .order('album', { ascending: true})
      setAlbums(data)
    }
    getData()
  }, [supabase])

  return <pre>{JSON.stringify(albums, null, 2)}</pre>
}