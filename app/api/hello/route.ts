import { NextRequest, NextResponse } from "next/server";

type ResponseData = {
  message: string
}

/**
 * @swagger
 * /api/hello:
 *   get:
 *     description: Returns the hello world
 *     responses:
 *       200:
 *         description: hello world
 */
export async function GET(
  _: NextRequest
) {
  try {
    const result = await someAsyncOperation();
    return NextResponse.json(
      { message: result },
      { status: 200 }
    );
  } catch (err) {
    NextResponse.json(
      { message: 'failed to load data' },
      { status: 500 }
    )
  }
}

function someAsyncOperation() {
  return Promise.resolve("Hello World");
}