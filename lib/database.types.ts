export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export interface Database {
  public: {
    Tables: {
      album: {
        Row: {
          created_at: string | null
          id: number
          name: Json | null
        }
        Insert: {
          created_at?: string | null
          id?: number
          name?: Json | null
        }
        Update: {
          created_at?: string | null
          id?: number
          name?: Json | null
        }
        Relationships: []
      }
      mp3: {
        Row: {
          abs_path: string
          album: string | null
          album_artist: string | null
          artist: string | null
          bitrate_in_kbps: number | null
          comment: string | null
          composer: string | null
          copyright: string | null
          encoder: string | null
          external_id: string
          file_name: string | null
          file_size_in_bytes: number | null
          genre: number | null
          genre_description: string | null
          id: number
          length_in_seconds: number | null
          original_artist: string | null
          publisher: string | null
          sample_rate_in_hz: number | null
          title: string | null
          track: string | null
          url: string | null
          variable_bitrate: boolean | null
          year: string | null
        }
        Insert: {
          abs_path: string
          album?: string | null
          album_artist?: string | null
          artist?: string | null
          bitrate_in_kbps?: number | null
          comment?: string | null
          composer?: string | null
          copyright?: string | null
          encoder?: string | null
          external_id: string
          file_name?: string | null
          file_size_in_bytes?: number | null
          genre?: number | null
          genre_description?: string | null
          id?: number
          length_in_seconds?: number | null
          original_artist?: string | null
          publisher?: string | null
          sample_rate_in_hz?: number | null
          title?: string | null
          track?: string | null
          url?: string | null
          variable_bitrate?: boolean | null
          year?: string | null
        }
        Update: {
          abs_path?: string
          album?: string | null
          album_artist?: string | null
          artist?: string | null
          bitrate_in_kbps?: number | null
          comment?: string | null
          composer?: string | null
          copyright?: string | null
          encoder?: string | null
          external_id?: string
          file_name?: string | null
          file_size_in_bytes?: number | null
          genre?: number | null
          genre_description?: string | null
          id?: number
          length_in_seconds?: number | null
          original_artist?: string | null
          publisher?: string | null
          sample_rate_in_hz?: number | null
          title?: string | null
          track?: string | null
          url?: string | null
          variable_bitrate?: boolean | null
          year?: string | null
        }
        Relationships: []
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}

export type Tables<
  PublicTableNameOrOptions extends
    | keyof (Database["public"]["Tables"] & Database["public"]["Views"])
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof (Database[PublicTableNameOrOptions["schema"]]["Tables"] &
        Database[PublicTableNameOrOptions["schema"]]["Views"])
    : never = never
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? (Database[PublicTableNameOrOptions["schema"]]["Tables"] &
      Database[PublicTableNameOrOptions["schema"]]["Views"])[TableName] extends {
      Row: infer R
    }
    ? R
    : never
  : PublicTableNameOrOptions extends keyof (Database["public"]["Tables"] &
      Database["public"]["Views"])
  ? (Database["public"]["Tables"] &
      Database["public"]["Views"])[PublicTableNameOrOptions] extends {
      Row: infer R
    }
    ? R
    : never
  : never

export type TablesInsert<
  PublicTableNameOrOptions extends
    | keyof Database["public"]["Tables"]
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicTableNameOrOptions["schema"]]["Tables"]
    : never = never
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? Database[PublicTableNameOrOptions["schema"]]["Tables"][TableName] extends {
      Insert: infer I
    }
    ? I
    : never
  : PublicTableNameOrOptions extends keyof Database["public"]["Tables"]
  ? Database["public"]["Tables"][PublicTableNameOrOptions] extends {
      Insert: infer I
    }
    ? I
    : never
  : never

export type TablesUpdate<
  PublicTableNameOrOptions extends
    | keyof Database["public"]["Tables"]
    | { schema: keyof Database },
  TableName extends PublicTableNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicTableNameOrOptions["schema"]]["Tables"]
    : never = never
> = PublicTableNameOrOptions extends { schema: keyof Database }
  ? Database[PublicTableNameOrOptions["schema"]]["Tables"][TableName] extends {
      Update: infer U
    }
    ? U
    : never
  : PublicTableNameOrOptions extends keyof Database["public"]["Tables"]
  ? Database["public"]["Tables"][PublicTableNameOrOptions] extends {
      Update: infer U
    }
    ? U
    : never
  : never

export type Enums<
  PublicEnumNameOrOptions extends
    | keyof Database["public"]["Enums"]
    | { schema: keyof Database },
  EnumName extends PublicEnumNameOrOptions extends { schema: keyof Database }
    ? keyof Database[PublicEnumNameOrOptions["schema"]]["Enums"]
    : never = never
> = PublicEnumNameOrOptions extends { schema: keyof Database }
  ? Database[PublicEnumNameOrOptions["schema"]]["Enums"][EnumName]
  : PublicEnumNameOrOptions extends keyof Database["public"]["Enums"]
  ? Database["public"]["Enums"][PublicEnumNameOrOptions]
  : never
