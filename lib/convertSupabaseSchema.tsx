// https://github.com/supabase/supabase/issues/9215
const fs = require("fs/promises");
const path = require("path");
const iconv = require("iconv-lite");
const chardet = require("chardet");

const inputFile = path.resolve("./lib/database.types.ts");
// Override
const outputFile = path.resolve("./lib/database.types.ts");

(async () => {
  const inputData = await fs.readFile(inputFile);

  // Convert to UTF-8 only when the character set is UTF-16 LE
  if (chardet.detect(inputData) === "UTF-16LE") {
    const outputData = iconv.decode(inputData, "utf16-le", "utf-8");
    await fs.writeFile(outputFile, outputData, "utf-8");
  }
})();